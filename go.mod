module gitlab.com/spbstu-2023/spbstu-2023-shpc-kur-mpi

go 1.21.4

require (
	github.com/gorilla/mux v1.8.1
	gitlab.com/spbstu-2023/spbstu-2023-shpc-kur-mpi-formula v0.0.0-20231215062659-d1eeb36174af
)

require (
	github.com/sandertv/go-formula/v2 v2.0.0-alpha.7 // indirect
	github.com/sirupsen/logrus v1.9.3 // indirect
	golang.org/x/sys v0.15.0 // indirect
	golang.org/x/xerrors v0.0.0-20231012003039-104605ab7028 // indirect
)
