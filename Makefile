include build/utils.mk

gobuild:
	export CGO_LDFLAGS='-L/usr/lib/ -lmpich'
	go build -o bin/main cmd/server/main.go

gorun:
	mpirun -np ${np} ./bin/main

gossa:
	GOSSAFUNC=timerMiddleware go build cmd/server/main.go
