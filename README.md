# spbstu-2023-shpc-kur-mpi
Сервис, создающий `http.Server` и использующий `spbstu-2023-shpc-kur-mpi-formula` 
для работы `go-formula` в распределенном режиме.

## About

Author: Andrianov Artemii

Group: #5140904/30202

## Prerequisite
- [Go 1.21](https://tip.golang.org/doc/go1.21)
- [Docker](https://www.docker.com/)
- gitlab.com/spbstu-2023/spbstu-2023-shpc-kur-mpi-formula v0.0.0-20231215062659-d1eeb36174af и выше

## Solution
Приложение стартует по схеме, когда `rank == 0` отвечает за прием запросов, разделение полученных данных 
и отправку их всем остальным процессорам с `rank != 0`. Каждый процесс производит свою часть вычислений 
и отправляет их процессу `rank == 0`. Как только все вычисления будут с аккамулированы в `rank == 0` процессе - 
ответ отправляется http клиенту.

![diagram](assets/diagram.png)

## Build
Первым шагом требуется собрать контейнер для запуска всех Makefile таргетов:
```bash
    make docker-build-container
```

Для сборки сервиса в исполняемый файл использовать:
```bash
    make docker-gobuild
```

## Run
Для запуска сервиса использовать 
```bash
    make docker-gorun np=<count-of-proc>
```

## SSA
Для создания файла с AST, SSA и прочими шаги сборки использовать
```bash
    make docker-gossa func=<name-of-func> file=<name-of-func-file>
```