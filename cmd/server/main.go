package main

import "C"
import (
	"bufio"
	"fmt"
	"github.com/gorilla/mux"
	formula "gitlab.com/spbstu-2023/spbstu-2023-shpc-kur-mpi-formula"
	"log"
	"net/http"
	"os"
	"os/signal"
	"time"
)

const rootRank = 0

func timerMiddleware(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		start := time.Now()
		next.ServeHTTP(w, r)
		duration := time.Since(start)
		w.Header().Add("duration", fmt.Sprintf("%dus", duration.Microseconds()))
	})

}

type h struct {
	size int
	rank int
}

func (h *h) handle(w http.ResponseWriter, r *http.Request) {
	scanner := bufio.NewScanner(r.Body)
	lines := make([]string, 0)
	for scanner.Scan() {
		lines = append(lines, scanner.Text())
	}
	results := formula.HandleCalculations(h.size, lines)

	var data string
	for _, result := range results {
		data += result
	}

	fmt.Fprintf(w, data)
}

func main() {
	sign := make(chan os.Signal)
	signal.Notify(sign, os.Interrupt, os.Kill)

	size, rank := formula.Init()
	defer formula.Finalize()

	if rank == 0 {
		h := h{
			size: size,
			rank: rank,
		}
		mux := mux.NewRouter()
		mux.HandleFunc("/calculate", h.handle).Methods(http.MethodOptions, http.MethodPost)
		mux.Use(timerMiddleware)

		server := http.Server{
			Addr:    ":8080",
			Handler: mux,
		}
		go server.ListenAndServe()

		<-sign
	} else {
		go func() {
			for {
				formula.ParallelCalculate(rank, rootRank, ";")
			}
		}()

		<-sign
		log.Printf("rank=%d: end of work with using signal", rank)
	}
}
