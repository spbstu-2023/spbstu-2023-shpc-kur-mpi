docker_container = golang-openmpi
docker_workdir = /usr/src/tmp
docker_args = --rm \
	-w ${docker_workdir} \
	-v ${PWD}:${docker_workdir}

np=4
args=
target=

docker-build-container:
	@docker build -t ${docker_container} -f build/Dockerfile . 

docker-%:
	@echo "Important: command exec in docker container ${docker_container}"
	@echo "Run arguments ${args}"
	@docker run -it -p "8080:8080" ${docker_args} ${docker_container} make ${*} np="${np}" args="${args}" target="${target}"
	@echo "Terminate container ${docker_container}"

docker-bash:
	@echo "Important: open bash in docker container ${docker_container}"
	@docker run -it ${docker_args} ${docker_container} bash ${args}
	@echo "Terminate container ${docker_container}"

